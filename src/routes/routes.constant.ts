export const NAVIGATION_ROUTES = {
  DASHBOARD: '/',
  /** NOTE: ------->  use "snake_case"  while naming the routes*/
  BUTTON: '/button',
  FORM_FIELD: '/form_field',
  LOGIN: '/login',
};
