import { DashboardIcon, LogoutIcon } from '@codeHimalaya/assets/svgs';
import { NAVIGATION_ROUTES } from '@codeHimalaya/routes/routes.constant';

const navItems = [
  {
    name: 'Dashboard',
    to: NAVIGATION_ROUTES.DASHBOARD,
    icon: DashboardIcon,
    // Sometime you will have to disable some view to the user
    // this visible boolean will be used in such scenario
    // TODO: needs discussion if this is actually good approach or
    visible: true,
  },
  {
    name: 'Hidden Page',
    to: NAVIGATION_ROUTES.DASHBOARD,
    icon: DashboardIcon,
    // Sometime you will have to disable some view to the user
    // this visible boolean will be used in such scenario
    // TODO: needs discussion if this is actually good approach or
    visible: false,
  },
  {
    name: 'Components',
    to: '',
    icon: DashboardIcon,
    visible: true,
    child: [
      {
        name: 'Button',
        to: NAVIGATION_ROUTES.BUTTON,
        icon: DashboardIcon,
        visible: true,
      },
      {
        name: 'Form',
        to: NAVIGATION_ROUTES.FORM_FIELD,
        icon: DashboardIcon,
        visible: true,
      },
    ],
  },
  {
    name: 'Logout',
    to: NAVIGATION_ROUTES.LOGIN,
    icon: LogoutIcon,
    visible: true,
  },
];

export { navItems };
